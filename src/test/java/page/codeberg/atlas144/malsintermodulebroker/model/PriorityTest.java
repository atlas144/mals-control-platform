// SPDX-License-Identifier: LGPL-2.1-only

package page.codeberg.atlas144.malsintermodulebroker.model;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 *
 * @author atlas144
 */
public class PriorityTest {
    
    /**
     * Test of getValue method, of class Priority.
     */
    @Test
    public void testGetValue() {
        Priority testPriority = Priority.NORMAL;
        byte expResult = 1;
        byte result = testPriority.getValue();
        
        assertEquals(expResult, result);
    }
    
}
