// SPDX-License-Identifier: LGPL-2.1-only

package page.codeberg.atlas144.malsintermodulebroker.subscriptions;

import static org.junit.Assert.assertNotNull;
import org.junit.Test;

/**
 *
 * @author atlas144
 */
public class SubscriptionsTrieInjectorTest {
    
    @Test
    public void testInitialization() {
        SubscriptionsTrieInjector subscriptionsTrieInjector = new SubscriptionsTrieInjector();
        
        assertNotNull(subscriptionsTrieInjector);
    }
    
    @Test
    public void testGetSubscriptionsTrie() {
        SubscriptionsTrieInjector.getSubscriptionsTrie();
    }
    
}
