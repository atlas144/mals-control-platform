// SPDX-License-Identifier: LGPL-2.1-only

package page.codeberg.atlas144.malsintermodulebroker.subscriptions;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import page.codeberg.atlas144.malsintermodulebroker.TaskModule;

/**
 *
 * @author atlas144
 */
@RunWith(MockitoJUnitRunner.class)
public class SubscriptionsTrieNodeTest {
    
    private SubscriptionsTrieNode<TaskModule> subscriptionsTrieNode;
    private Map children;
    private Set<TaskModule> subscribers;
    @Mock
    private TaskModule subscriber;
    
    @Before
    public void setup() {
        subscribers = new HashSet<>();
        
        subscriptionsTrieNode = new SubscriptionsTrieNode(children, subscribers);
    }
    
    @Test
    public void testGetChildren() {
        assertEquals(children, subscriptionsTrieNode.getChildren());
    }

    @Test
    public void testGetSubscribers() {
        assertEquals(subscribers, subscriptionsTrieNode.getSubscribers());
    }
    
    @Test
    public void testAddSubscriber() {
        subscriptionsTrieNode.addSubscriber(subscriber);
        
        assertEquals(subscribers.size(), 1);
    }

    @Test
    public void testRemoveSubscriber() {
        subscribers.add(subscriber);
        subscriptionsTrieNode.removeSubscriber(subscriber);
        
        assertEquals(subscribers.size(), 0);
    }
    
}
