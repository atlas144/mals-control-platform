// SPDX-License-Identifier: LGPL-2.1-only

package page.codeberg.atlas144.malsintermodulebroker;

import java.util.List;
import java.util.concurrent.ConcurrentMap;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import page.codeberg.atlas144.malsintermodulebroker.intercommunication.IntercommunicationModule;
import page.codeberg.atlas144.malsintermodulebroker.model.Priority;

/**
 * Platform for controlling of robbotic system. It provides a connection between
 * the system modules and communication with the user application.
 *
 * @author atlas144
 */
public class MalsIntermoduleBroker {

    private final Logger logger;
    private final ConcurrentMap<String, TaskModule> taskModules;
    private final IntercommunicationModule intercommunicationModule;
    
    /**
     * Creates platform instance.
     * @param taskModules map for storing task modules
     * @param intercommunicationModule broker module
     */
    public MalsIntermoduleBroker(ConcurrentMap<String, TaskModule> taskModules, IntercommunicationModule intercommunicationModule) {
        logger = LoggerFactory.getLogger(MalsIntermoduleBroker.class);
        this.taskModules = taskModules;
        this.intercommunicationModule = intercommunicationModule;
    }
    
    /**
     * Registers new module in the platform.
     * 
     * @param newModule module to be registered
     */
    public void registerModule(TaskModule newModule) {
        taskModules.put(newModule.getModuleName(), newModule);
        newModule.registerBroker(this);
        logger.debug("Task module '{}' successfully registered", newModule.getModuleName());
    }
    
    private <T> JSONObject constructSimplePayload(T value) {
        JSONObject payload = new JSONObject();
        
        payload.put("value", value);
        
        return payload;
    }
    
    /**
     * Passes the message to the broker so it can send it to its subscribers.
     * The given payload is assigned to the key named 'value'.
     * 
     * @param topic topic of the message
     * @param payload message content
     * @param priority message priority
     */
    public void publish(String topic, String payload, Priority priority) {
        intercommunicationModule.publish(topic, constructSimplePayload(payload), priority);
    }
    
    /**
     * Passes the message to the broker so it can send it to its subscribers.
     * The given payload is assigned to the key named 'value'.
     * 
     * @param topic topic of the message
     * @param payload message content
     * @param priority message priority
     */
    public void publish(String topic, double payload, Priority priority) {
        intercommunicationModule.publish(topic, constructSimplePayload(payload), priority);
    }
    
    /**
     * Passes the message to the broker so it can send it to its subscribers.
     * The given payload is assigned to the key named 'value'.
     * 
     * @param topic topic of the message
     * @param payload message content
     * @param priority message priority
     */
    public void publish(String topic, boolean payload, Priority priority) {
        intercommunicationModule.publish(topic, constructSimplePayload(payload), priority);
    }
    
    /**
     * Passes the message to the broker so it can send it to its subscribers.
     * 
     * @param topic topic of the message
     * @param payload message content
     * @param priority message priority
     */
    public void publish(String topic, JSONObject payload, Priority priority) {
        intercommunicationModule.publish(topic, payload, priority);
    }
    
    /**
     * Passes the message to the broker so it can send it to its subscribers.
     * 
     * @param topic topic of the message
     * @param priority message priority
     */
    public void publish(String topic, Priority priority) {
        intercommunicationModule.publish(topic, new JSONObject(), priority);
    }
    
    /**
     * Sets subscription to a given topic.
     * 
     * @param topic topic to be subscribed to
     * @param subscriber module which is subscribing
     */
    public void subscribe(String topic, TaskModule subscriber) {
        intercommunicationModule.subscribe(topic, subscriber);
    }
    
    /**
     * Sets subscription to a given topics.
     * 
     * @param topics list of topics to be subscribed to
     * @param subscriber module which is subscribing
     */
    public void subscribe(List<String> topics, TaskModule subscriber) {
        intercommunicationModule.subscribe(topics, subscriber);
    }
    
    /**
     * Ends subscription to a given topic.
     * 
     * @param topic topic to be unsubscribed to
     * @param subscriber task module which is canceling the subscribtion
     */
    public void unsubscribe(String topic, TaskModule subscriber) {
        intercommunicationModule.unsubscribe(topic, subscriber);
    }
    
    /**
     * Starts all components and the platform itself.
     */
    public void start() {
        intercommunicationModule.start();
        logger.debug("Intercommunication module successfully started");
        
        for (TaskModule taskModule : taskModules.values()) {
            taskModule.start();
            logger.debug("Task module '{}' successfully started", taskModule.getModuleName());
        }
    }
    
}
