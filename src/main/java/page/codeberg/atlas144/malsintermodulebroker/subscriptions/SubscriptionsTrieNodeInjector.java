// SPDX-License-Identifier: LGPL-2.1-only

package page.codeberg.atlas144.malsintermodulebroker.subscriptions;

import java.util.HashMap;
import java.util.HashSet;

/**
 *
 * @author atlas144
 */
public class SubscriptionsTrieNodeInjector {
    
    public static SubscriptionsTrieNode getSubscriptionsTrieNode() {
        return new SubscriptionsTrieNode(new HashMap(), new HashSet<>());
    }
    
}
