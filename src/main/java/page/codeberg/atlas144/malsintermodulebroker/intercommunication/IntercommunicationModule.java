// SPDX-License-Identifier: LGPL-2.1-only

package page.codeberg.atlas144.malsintermodulebroker.intercommunication;

import java.util.List;
import java.util.concurrent.BlockingQueue;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import page.codeberg.atlas144.malsintermodulebroker.TaskModule;
import page.codeberg.atlas144.malsintermodulebroker.subscriptions.SubscriptionsTrie;
import page.codeberg.atlas144.malsintermodulebroker.exceptions.UnregisteredTopicException;
import page.codeberg.atlas144.malsintermodulebroker.model.Message;
import page.codeberg.atlas144.malsintermodulebroker.model.Priority;

/**
 * A module that acts as a broker for transferring messages between other 
 * components of the platform.
 * It allows the component to subscribe to a topic and then forwards messages 
 * with that topic to it.
 *
 * @author atlas144
 */
public class IntercommunicationModule extends Thread {
    
    private final Logger logger;
    private final BlockingQueue<Message> messageQueue;
    private final SubscriptionsTrie<TaskModule> subscriptions;
        
    /**
     * Sends messages to subscribers of corresponding topic.
     * 
     * @param message message to be sent
     */
    private void sendMessage(Message message) {
        int counter = 0;
            
        try {
            for (TaskModule module : subscriptions.getSubscribers(message.topic())) {
                module.acceptMessage(message);
                logger.trace("Message '{}' successfully sent to the '{}' task module", message, module.getModuleName());
                counter++;
            }
        } catch (UnregisteredTopicException exception) {
            logger.trace(exception.getMessage());
        }
            
        logger.debug("Message '{}' successfully sent to {} modules", message, counter);
    }
    
    /**
     * Creates module.
     * @param messageQueue queue of messages passed to the module
     * @param subscriptions trie structure containing subscriptions
     */
    public IntercommunicationModule(BlockingQueue<Message> messageQueue, SubscriptionsTrie<TaskModule> subscriptions) {
        logger = LoggerFactory.getLogger(IntercommunicationModule.class);
        this.messageQueue = messageQueue;
        this.subscriptions = subscriptions;
        logger.debug("Intercommunication module successfully created");
    }
    
    /**
     * Passes the message to the broker so it can send it to its subscribers.
     * 
     * @param topic topic of the message
     * @param payload message content
     * @param priority message priority
     */
    public void publish(String topic, JSONObject payload, Priority priority) {
        messageQueue.add(new Message(topic, payload, priority));
        logger.trace("Message successfully published\nTopic: {}\nPayload: {}\nPriority: {}", topic, payload, priority.toString());
    }
    
    /**
     * Sets subscription to a given topic.
     * 
     * @param topic topic to be subscribed to
     * @param subscriber module which is subscribing
     */
    public void subscribe(String topic, TaskModule subscriber) {
        subscriptions.subscribe(topic, subscriber);
        logger.debug("Task module '{}' successfully subscribed to '{}' topic", subscriber.getModuleName(), topic);
    }
    
    /**
     * Sets subscription to a given topics.
     * 
     * @param topics list of topics to be subscribed to
     * @param subscriber module which is subscribing
     */
    public void subscribe(List<String> topics, TaskModule subscriber) {
        for (String topic : topics) {
            subscriptions.subscribe(topic, subscriber);
        }
        
        logger.debug("Task module '{}' successfully subscribed to '{}' topics", subscriber.getModuleName(), topics);
    }
    
    /**
     * Ends subscription to a given topic.
     * 
     * @param topic topic to be unsubscribed to
     * @param subscriber task module which is canceling the subscribtion
     */
    public void unsubscribe(String topic, TaskModule subscriber) {
        try {
            subscriptions.unsubscribe(topic, subscriber);
            logger.debug("Task module '{}' successfully unsubscribed '{}' topic", subscriber.getModuleName(), topic);
        } catch (UnregisteredTopicException ex) {
            logger.debug("Task module '{}' has not been subscribing to '{}' topic", subscriber.getModuleName(), topic);
        }
    }
    
    /**
     * Broker action code. Sends messages from queue to the subscribers. 
     */
    @Override
    public void run() {
        while (true) {            
            try {
                sendMessage(messageQueue.take());
            } catch (InterruptedException ex) {
                logger.debug("Intercommunication module stopping");
                return;
            }
        }
    }
    
}
