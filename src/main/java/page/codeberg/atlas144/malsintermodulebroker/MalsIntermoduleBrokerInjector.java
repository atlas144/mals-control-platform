// SPDX-License-Identifier: LGPL-2.1-only

package page.codeberg.atlas144.malsintermodulebroker;

import java.util.concurrent.ConcurrentHashMap;
import page.codeberg.atlas144.malsintermodulebroker.intercommunication.IntercommunicationModuleInjector;

/**
 *
 * @author atlas144
 */
public class MalsIntermoduleBrokerInjector {
    
    public static MalsIntermoduleBroker getMalsIntermoduleBroker() {
        return new MalsIntermoduleBroker(
            new ConcurrentHashMap<>(),
            IntercommunicationModuleInjector.getIntercommunicationModule()
        );
    }
    
}
